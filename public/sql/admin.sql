DROP TABLE IF EXISTS  admin;

CREATE TABLE admin(id SERIAL PRIMARY KEY NOT NULL, gebruikersnaam VARCHAR(30) UNIQUE NOT NULL, wachtwoord VARCHAR(255) NOT NULL);
INSERT INTO admin(gebruikersnaam, wachtwoord) VALUES ("admin", "wachtwoord");
