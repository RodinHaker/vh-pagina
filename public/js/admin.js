window.onload = function () {
    const minimize_btn = document.getElementById("minimize"),
        maximize_btn = document.getElementById("maximize"),
        leftnav = document.getElementById("left-nav"),
        topnav = document.getElementById("top-nav"),
        main = document.getElementById("main"),
        home = document.getElementById("home"),
        huurders = document.getElementById("huurders"),
        vragen = document.getElementById("vragen"),
        home_act = document.getElementById("home-action"),
        huurders_act = document.getElementById("huurders-action"),
        vragen_act = document.getElementById("vragen-action"),
        buttons = [home, huurders, vragen],
        actions = [home_act, huurders_act, vragen_act],
        vragen_table = document.getElementById("vragen-table"),
        edit_buttons = document.getElementsByName("edit-btn"),
        delete_buttons = document.getElementsByName("delete-btn"),
        save_buttons = document.getElementsByName("save-btn"),
        cancel_buttons = document.getElementsByName("cancel-btn"),
        save_btns = document.getElementsByClassName("save-btns");

    let originalValue;

    // window.addEventListener("keypress", function (e) {
    //     if (e.key === "Enter") {
    //         this.closest("form").submit();
    //     }
    // }, false);

    minimize_btn.addEventListener("click", minimize, false);
    maximize_btn.addEventListener("click", maximize, false);

    function minimize() {
        leftnav.style.left = "-200px";
        topnav.style.left = "0px";
        topnav.style.width = "100%";
        main.style.left = "0px";
        main.style.width = "100%";
        maximize_btn.style.transform = "scale(1)";
    }

    function maximize() {
        leftnav.style.left = "0px";
        topnav.style.left = "200px";
        topnav.style.width = "calc(100% - 200px)";
        main.style.left = "200px";
        main.style.width = "calc(100% - 200px)";
        maximize_btn.style.transform = "scale(0)";
    }

    function show(index){
        for(let i = 0; i < actions.length; i++) {
            if(i !== index) {
                actions[i].style.display = "none";
            }else{
                actions[i].style.display = "block";
            }
        }
    }

    show(0);

    for(let i = 0; i < actions.length; i++) {
        (function() {
            buttons[i].addEventListener("click", function() {show(i); }, false);
        }());
    }

    for(let i = 0; i < edit_buttons.length; i++) {
        (function() {
            edit_buttons[i].addEventListener("click", function() {editRow(edit_buttons[i]); }, false);
        }());
    }

    for(let i = 0; i < cancel_buttons.length; i++) {
        (function() {
            cancel_buttons[i].addEventListener("click", function() {cancelEditing(cancel_buttons[i]); }, false);
        }());
    }

    function hideSaveCancelButtons() {
        for(let i = 0; i < save_btns.length; i++) {
            save_btns[i].style.display = "none";
        }
    }

    hideSaveCancelButtons();

    function editRow(btn) {
        let row = btn.parentElement.parentElement.parentElement,
            tds = row.getElementsByTagName("td");
        tds[3].getElementsByTagName("div")[0].style.display = "none";
        tds[3].getElementsByTagName("div")[1].style.display = "inline-flex";

        originalValue = tds[1].childNodes[0].nodeValue;

        let temp1 = document.createElement("td");
        let temp2 = document.createElement("input");
        temp2.setAttribute('type', 'text');
        temp2.value = originalValue;
        temp1.appendChild(temp2);

        tds[1].parentNode.replaceChild(temp1, tds[1]);
    }

    function cancelEditing(btn) {
        let row = btn.parentElement.parentElement.parentElement,
            tds = row.getElementsByTagName("td");

        tds[3].getElementsByTagName("div")[0].style.display = "inline-flex";
        tds[3].getElementsByTagName("div")[1].style.display = "none";

        let temp1 = document.createElement("td");
        temp1.textContent = originalValue;

        tds[1].parentNode.replaceChild(temp1, tds[1]);
    }
}

