const fetch = require("node-fetch");

const interval = 25 * 60 * 1000;
const url = "https://vh-pagina.herokuapp.com/";

(function wake() {

    try {

        const handler = setInterval(() => {

            fetch(url)
                .then(res => console.log(`response-ok: ${res.ok}, status: ${res.status}`))
                    .catch(err => console.error(`Error occured: ${err}`));

        }, interval);

    } catch (err) {
        console.error("Error occured: retrying...");
        clearInterval(handler);
        return setTimeout(() => wake(), 10000);
    }

})();
