const db = require("../lib/db")
const bcrypt = require("bcrypt")

const createAdminTable = async () => {
    await db.query("DROP TABLE IF EXISTS admins;");
    await db.query("CREATE TABLE admins(id SERIAL PRIMARY KEY NOT NULL, gebruikersnaam VARCHAR(30) NOT NULL, wachtwoord VARCHAR(255) NOT NULL);");

    const saltRounds = 10;
    const plainTextPassword1 = "DFGh5546*%^__90";

    bcrypt
        .hash(plainTextPassword1, saltRounds)
        .then(hash => {
            db.query("INSERT INTO admins(gebruikersnaam, wachtwoord) VALUES ('admin', $1);", [hash]);
        })
        .catch(err => console.error(err.message));
}

const createVragenTable = async () => {
    await db.query("CREATE TABLE IF NOT EXISTS vragen(id SERIAL PRIMARY KEY NOT NULL, vraag TEXT NOT NULL, type SMALLINT NOT NULL);");
}

const dropAdminTable = async (callback) => {
    await db.query("DROP TABLE IF EXISTS admins;");
}

const dropVragenTable = async () => {
    await db.query("DROP TABLE IF EXISTS vragen;");
}

const createAllTables = () => {
    createAdminTable();
    createVragenTable();
}

const dropAllTables = () => {
    dropAdminTable();
    dropVragenTable();
}

module.exports = {
    createAdminTable,
    createVragenTable,
    dropAdminTable,
    dropVragenTable,
    createAllTables,
    dropAllTables
};

require('make-runnable')
