const express = require("express");
const app = express();
const passport = require("passport");
const os = require("os");
const request = require("request");
const connectEnsureLogin = require("connect-ensure-login");
const db = require("../lib/db")
const bcrypt = require("bcrypt")

app.use(express.static("public"));

const LocalStrategy = require("passport-local").Strategy;

module.exports = function (app) {
// Home screen
    app.get("/", function (req, res, next) {
        res.render("index", {
            title: "Home",
            messages: {danger: req.flash("danger"), warning: req.flash("warning"), success: req.flash("success")}
        });
    });

// Admin page
    app.get("/admin", function (req, res, next) {
        if (req.isAuthenticated()) {
            db.query("SELECT * FROM vragen", [], (err, rows, callback) => {
                if(err) throw err

                res.render("admin", {
                    title: "Admin Pagina",
                    userData: req.user,
                    messages: {danger: req.flash("danger"), warning: req.flash("warning"), success: req.flash("success")},
                    vragen: rows.rows
                });
            })

        } else {
            res.redirect("/admin_login");
        }
    });

// Admin login
    app.get("/admin_login", function (req, res, next) {
        if (req.isAuthenticated()) {
            res.redirect("/admin");
        } else {
            res.render("admin_login", {
                title: "Admin login",
                userData: req.user,
                messages: {danger: req.flash("danger"), warning: req.flash("warning"), success: req.flash("success")}
            });
        }

    });

    app.post("/admin_login", passport.authenticate("local", {
        successRedirect: "/admin",
        failureRedirect: "/admin_login",
        failureFlash: true
    }), function (req, res) {
        if (req.body.remember) {
            req.session.cookie.maxAge = 30 * 24 * 60 * 60 * 1000; // Cookie expires after 30 days
        } else {
            req.session.cookie.expires = false; // Cookie expires at end of session
        }
        res.redirect("/");
    });

// Logout
    app.get("/logout", function (req, res) {

        console.log(req.isAuthenticated());
        req.logout();
        console.log(req.isAuthenticated());
        req.flash("success", "Uitgelogd!");
        res.redirect("/admin_login");
    });
}

// Login magic
passport.use("local", new LocalStrategy({passReqToCallback: true}, async (req, username, password, done) => {

        loginAttempt();

        async function loginAttempt() {

            try {
                await db.query("BEGIN")
                console.log(username + password)
                // const currentAccountsData = await JSON.stringify(
                await db.query("SELECT id, gebruikersnaam, wachtwoord FROM admins WHERE gebruikersnaam=$1", [username], async (err, result) => {
                    console.log("1")
                    if (err) {
                        return done(err)
                    }
                    if (result.rows[0] == null) {
                        req.flash("danger", "Incorrecte login!");
                        return done(null, false);
                    } else {
                        console.log(result.rows[0].wachtwoord)
                        function comparePwd(password, hash) {
                            return new Promise(function (resolve, reject)
                            {
                                bcrypt.compare(password, hash, (err, check) => {
                                    if (err) {
                                        console.log("Fout bij het checken van het wachtwoord!");
                                        return done();
                                    } else if (check) {
                                        return done(null, [{
                                            id: result.rows[0].id,
                                            gebruikersnaam: result.rows[0].gebruikersnaam
                                        }]);
                                    } else {
                                        req.flash("danger", "Incorrecte login!");
                                        return done(null, false);
                                    }
                                });
                            });
                        }
                        const res = await comparePwd(password, result.rows[0].wachtwoord);
                    }
                });
                await db.query("COMMIT")
            } catch (e) {
                throw (e);
            }
        }
    }
))

passport.serializeUser(async (user, done) => {
    done(null, user);
});

passport.deserializeUser(async (user, done) => {
    done(null, user);
});	
